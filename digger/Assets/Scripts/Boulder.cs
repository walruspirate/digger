﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boulder : MonoBehaviour {

	public string whichSide;
	public Character charscr;

	BoxCollider2D collRef;
	Collider2D[] test = new Collider2D[1];
	ContactFilter2D test2 = new ContactFilter2D();
	public Collider2D currentCollider;

	void Start () {
		collRef = GetComponent<BoxCollider2D>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public bool isOverlapping(){
	if(collRef.OverlapCollider(test2, test) == 1){
			if(test[0].tag == "playerbounds"){
				currentCollider = test[0];
				return true;
			}
	}
	return false;
	}


/*	void OnTriggerEnter2D(Collider2D coll){

		if(coll.gameObject.tag == "playerbounds"){
			if(whichSide == "left"){
				charscr.blockedLeft = true;
			}
			if(whichSide == "right"){
				charscr.blockedRight = true;
			}
			if(whichSide == "top"){
				charscr.blockedTop = true;
			}
		}
	}

	void OnTriggerExit2D(Collider2D coll){


		if(coll.gameObject.tag == "playerbounds"){
			if(whichSide == "left"){
				if(collRef.OverlapCollider(test2, test) == 1)
					return;
				charscr.blockedLeft = false;
			}
			if(whichSide == "right"){
				if(collRef.OverlapCollider(test2, test) == 1)
					return;
				charscr.blockedRight = false;
			}
			if(whichSide == "top"){
				if(collRef.OverlapCollider(test2, test) == 1)
					return;
				charscr.blockedTop = false;
						
			}
		
	}
	} */
}
