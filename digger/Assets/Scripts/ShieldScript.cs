﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldScript : MonoBehaviour {

	public GameObject shield;
	Character charscr;

void OnTriggerEnter2D(Collider2D coll){

		if(coll.tag == "playercharacter"){
			charscr = coll.GetComponent<Character>();
			charscr.shieldAvailable = true;
			GameObject temp = Instantiate(shield, coll.gameObject.transform.position + new Vector3(0, 0.43f), Quaternion.identity);
			charscr.shield = temp;
			temp.transform.SetParent(coll.gameObject.transform);
			Destroy(gameObject);
		}
	}
}
