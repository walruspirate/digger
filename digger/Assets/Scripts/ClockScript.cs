﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ClockScript : MonoBehaviour {

	incrementer inc;
	Tween animTween;

void OnTriggerEnter2D(Collider2D coll){

		if(coll.tag == "playercharacter"){
			inc = Camera.main.GetComponent<incrementer>();
			animTween = DOTween.To(x => inc.speed = x, inc.speed, (inc.speed - 5), 2f);
			StartCoroutine(TweenRunning());
			gameObject.GetComponent<SpriteRenderer>().sprite = null;
		}
	}

IEnumerator TweenRunning(){
	while(animTween.IsPlaying() == true){
		if(inc.speed < 1){
			animTween.Kill();
		}
		yield return null;
	}
	Destroy(gameObject);
}
}
