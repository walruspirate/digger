﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterScroller : MonoBehaviour {

	incrementer incr;
	public float blockDistance;
	Vector3 deltaChange;

	public delegate void deltaBlock();

	public static event deltaBlock deltaevent;
	void Start () {
		incr = Camera.main.GetComponent<incrementer>();
		
	}
	
	void Update(){
		deltaChange = (new Vector3(0, incr.speed, 0)  * Time.deltaTime );
	}
	void LateUpdate () {
		//deltaChange = (new Vector3(0, incr.speed, 0)  * Time.deltaTime );
		transform.position = transform.position - deltaChange;
		blockDistance = blockDistance + deltaChange.y;
		if(blockDistance >= 6.05f){
			deltaevent();
			blockDistance = 0;
		}

		
	}

}
