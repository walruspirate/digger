﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionSnap : MonoBehaviour {


	public void snapUp(){
		transform.localPosition = transform.localPosition + new Vector3(0, 1.21f, 0);
	}

	public void snapLeft(){
		transform.localPosition = transform.localPosition + new Vector3(-1.21f, 0, 0);
	}

	public void snapRight(){
		transform.localPosition = transform.localPosition + new Vector3(1.21f, 0, 0);
	}
}
