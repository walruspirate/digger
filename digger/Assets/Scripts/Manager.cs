﻿using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class Manager : MonoBehaviour {


	void Start () {
		
		PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();
		PlayGamesPlatform.InitializeInstance(config);
		PlayGamesPlatform.Activate();
		SignIn();
		
	}
	
	public void SignIn(){

		Social.localUser.Authenticate(success => {});
		StartCoroutine(startach());
	}

	IEnumerator startach(){
		yield return new WaitForSeconds(10f);
		Social.ReportProgress("CgkIurSoi8wIEAIQAA", 100, success => {});
		Social.ShowAchievementsUI();
	}
	
	public void RestartLevel(){
		SceneManager.LoadScene(0);
	}

}
