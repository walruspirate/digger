﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemInstantiator : MonoBehaviour {

	public List<GameObject> positions;
	public List<GameObject> prefabs;
	float probability;
	List<GameObject> boulderList = new List<GameObject>();
	List<GameObject> spikesList =  new List<GameObject>();
	Vector2 boulderPosition;

	void Start () {
	//	boulderPosition = transform.position + new Vector3(-2.42f, 2.42f);
		for(int i = 0; i < 50; i++){
			GameObject obj = (GameObject)Instantiate(prefabs[1], transform.position, Quaternion.identity);
			GameObject obj2 = (GameObject)Instantiate(prefabs[4], transform.position, Quaternion.identity);
			obj.SetActive(false);
			obj2.SetActive(false);
			boulderList.Add(obj);
			spikesList.Add(obj2);
			
		}
		CharacterScroller.deltaevent += itemInstantiate;
	//	StartCoroutine(itemInstantiate());
	}
	
	// Update is called once per frame
	void itemInstantiate(){
	//	probability = Random.Range(0,1f);
		if(Random.Range(0,1f) < 0.5f){
			Instantiate(prefabs[0], RandomPosition(), Quaternion.identity); //GOLD			
		}
		if(Random.Range(0,1f) < 0.5f){
			Instantiate(prefabs[2], RandomPosition(), Quaternion.identity); //CLOCK
		}
		if(Random.Range(0,1f) < 0.5f){
			Instantiate(prefabs[3], RandomPosition(), Quaternion.identity); //SHIELD
		}

		if(Random.Range(0,1f) < 0.5f){
			Instantiate(prefabs[5], RandomPosition(), Quaternion.identity); //MULTIPLIER
		}

		//if(Random.Range(0,1f) > 0.3f){
			createBoulders();
	//	}
	}

	Vector3 RandomPosition(){
		return positions[Random.Range(0, positions.Count)].transform.position;
	}

	void createBoulders(){
		float prob = Random.Range(0,1f);
		if(prob > 0 && prob < 0.33f){
			boulderPosition = positions[0].transform.position;// + new Vector3(-2.42f, 2.42f);
		}
		if(prob > 0.33f && prob < 0.66f){
			boulderPosition = positions[2].transform.position;// + new Vector3(-2.42f, 2.42f);
		}
		if(prob > 0.66f && prob < 1f){
			boulderPosition = positions[4].transform.position;// + new Vector3(-2.42f, 2.42f);
		}
		for(int i = 0; i < 25; i++){		
			if(Random.Range(0f, 1f) < 0.5f){
				if(boulderList[i].activeInHierarchy == false){
					boulderList[i].transform.position = boulderPosition;
					boulderList[i].SetActive(true);
				}			
			} else if(Random.Range(0f, 1f) < 0.5f){
				if(spikesList[i].activeInHierarchy == false){
					spikesList[i].transform.position = boulderPosition;
					spikesList[i].SetActive(true);
				}

			}
			boulderPosition.x = boulderPosition.x + 1.21f;
			if((i+1) % 5 == 0){
				boulderPosition.x = boulderPosition.x - 6.05f;
				boulderPosition.y = boulderPosition.y - 1.21f;
			}
		}
	//	boulderPosition = transform.position + new Vector3(-2.42f, 2.42f);
	}
	
}
