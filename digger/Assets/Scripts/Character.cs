﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Character : MonoBehaviour {

	public incrementer incr;
	public PositionSnap posSnap;
	public float animationSpeed;
	int position = 0;

	public GameObject currSprite;
	public GameObject upPositionObject, leftPositionObject, rightPositionObject;
	public List<GameObject> tiles;
	Vector3 nextPosition;
	bool wasLastRight;
	public bool shieldAvailable = false;
	public GameObject shield;
	public int isGoingSideways = 0;
//	public bool blockedLeft = false, blockedRight = false, blockedTop = false;
	
	//public delegate void ThreadStart();

	void Start () {
		nextPosition = new Vector3(0, -0.3100001f, 0);
	
	}

	void Update(){
	
	}



	public void GoUp(){
			if(upPositionObject.transform.position.y > 8.73f)
				return;


			if(upPositionObject.GetComponent<Boulder>().isOverlapping() == true){
				if(shieldAvailable == true){
					upPositionObject.GetComponent<Boulder>().currentCollider.gameObject.SetActive(false);
					shieldAvailable = false;
					Destroy(shield);
				}else{
					return;
				}
			}
				
			isGoingSideways = 0;
			if(currSprite.GetComponent<SpriteRenderer>().sprite.name == "tile2" && wasLastRight == false){
				currSprite.GetComponent<SpriteRenderer>().sprite = tiles[5].GetComponent<SpriteRenderer>().sprite;
			}else if(currSprite.GetComponent<SpriteRenderer>().sprite.name == "tile2" && wasLastRight == true){
				currSprite.GetComponent<SpriteRenderer>().sprite = tiles[5].GetComponent<SpriteRenderer>().sprite;
				currSprite.transform.Rotate(0,180,0);
			}else{
				currSprite.GetComponent<SpriteRenderer>().sprite = tiles[1].GetComponent<SpriteRenderer>().sprite;
			}
			currSprite = Instantiate(tiles[0], upPositionObject.transform.position, Quaternion.identity);
			nextPosition = nextPosition + new Vector3(0,1.21f,0);
			transform.DOLocalMoveY(nextPosition.y, animationSpeed, false).SetEase(Ease.OutCubic);
			posSnap.snapUp();
	}

	

	public void GoLeft(){

			if(leftPositionObject.GetComponent<Boulder>().isOverlapping() == true){
				if(shieldAvailable == true){
					leftPositionObject.GetComponent<Boulder>().currentCollider.gameObject.SetActive(false);
					shieldAvailable = false;
					Destroy(shield);
				}else{
					return;
				}
			}
				

			if(isGoingSideways == 0 || isGoingSideways == -1){
		
				if(position>-4){
					if(currSprite.GetComponent<SpriteRenderer>().sprite.name == "tile1"){
					currSprite.GetComponent<SpriteRenderer>().sprite = tiles[2].GetComponent<SpriteRenderer>().sprite;
				}
				isGoingSideways = -1;
				wasLastRight = false;
					currSprite = Instantiate(tiles[4], leftPositionObject.transform.position, Quaternion.identity);
					nextPosition = nextPosition + new Vector3(-1.21f,0,0);
					transform.DOLocalMoveX(nextPosition.x, animationSpeed, false).SetEase(Ease.OutCubic);
					position--;
					posSnap.snapLeft();
				}
			}
		
	}

	public void GoRight(){

			if(rightPositionObject.GetComponent<Boulder>().isOverlapping() == true){
				if(shieldAvailable == true){
					rightPositionObject.GetComponent<Boulder>().currentCollider.gameObject.SetActive(false);
					shieldAvailable = false;
					Destroy(shield);
				}else{
					return;
				}
			}
				

			if(isGoingSideways == 0 || isGoingSideways == 1){
			
				
				if(position<4){
						if(currSprite.GetComponent<SpriteRenderer>().sprite.name == "tile1"){
					currSprite.GetComponent<SpriteRenderer>().sprite = tiles[2].GetComponent<SpriteRenderer>().sprite;
					currSprite.transform.Rotate(0,180,0);
				}
				isGoingSideways = 1;
				wasLastRight = true;
					currSprite = Instantiate(tiles[4], rightPositionObject.transform.position, Quaternion.identity);
					nextPosition = nextPosition + new Vector3(1.21f,0,0);
					transform.DOLocalMoveX(nextPosition.x, animationSpeed, false).SetEase(Ease.OutCubic);
					position++;
					posSnap.snapRight();
				}
			}	
	}


}
