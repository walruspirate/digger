﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonPress : MonoBehaviour{

	public Character charscr;
	public enum ButtonPosition{left, middle, right};
	public ButtonPosition button;

	public void ButtonPressed(){

		if(button == ButtonPosition.left)
			charscr.GoLeft();

		if(button == ButtonPosition.middle)
			charscr.GoUp();

		if(button == ButtonPosition.right)
			charscr.GoRight();


	}

}

