﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoulderDeactivator : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(transform.position.y < -9.7f)
			gameObject.SetActive(false);
	}
}
