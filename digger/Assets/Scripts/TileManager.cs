﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileManager : MonoBehaviour {

	incrementer incr;
	public bool isDestroyable = true;

	void Start () {
		incr = Camera.main.GetComponent<incrementer>();
		if(isDestroyable == true)
			Destroy(gameObject, 20f);
	}
	
	// Update is called once per frame
	void LateUpdate () {
		transform.position = transform.position - (new Vector3(0, incr.speed, 0)  * Time.deltaTime );
	}
}
