﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CoinManager : MonoBehaviour {

	public GameObject coinBar;

	BoxCollider2D selfCollider;

	CoinCollector coinColl;

	 Vector3[] positions = { new Vector3 { x = 10, y = 10, z = 10}, new Vector3(1,2,3) };

	 void OnEnable(){

		coinColl = Camera.main.GetComponent<CoinCollector>();
		selfCollider = GetComponent<BoxCollider2D>();
		coinBar = coinColl.coinBar;
	

	 }

	void OnTriggerEnter2D(Collider2D coll){

		if(coll.tag == "playercharacter"){
			selfCollider.enabled = false;
			transform.DOMove(new Vector3(-4.39f, 6.41f, 0), 1f).SetEase(Ease.InCubic).OnComplete(AnimComplete);
		}
	}

	public void RunCollection(){
		selfCollider.enabled = false;
		transform.DOMove(new Vector3(-4.39f, 6.41f, 0), 1f).SetEase(Ease.InCubic).OnComplete(AnimComplete);
	}

	public void AnimComplete(){
		coinColl.incrementCoin();
		coinColl.incrementCoinText();
		coinColl.AddCoinPoints();
		coinBar.transform.DOComplete();
		coinBar.transform.DOPunchScale(new Vector3(0.3f, 0.3f, 1), 0.4f, 1, 0);
		Destroy(gameObject);
	}
	
}
