﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinMultiplier : MonoBehaviour {

	// Use this for initialization
	void OnTriggerEnter2D(Collider2D coll){
		if(coll.tag == "playercharacter"){
			CoinCollector coincoll = Camera.main.GetComponent<CoinCollector>();
			coincoll.multiplierMoveIn();
			coincoll.multiplier = 2;
			coincoll.StartCoroutine(coincoll.multiplierDuration());
			Destroy(gameObject);
		}
	}
}
