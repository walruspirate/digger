﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class CoinCollector : MonoBehaviour {

	public int totalCoins;
	public int score;
	public Text coinText;
	public GameObject coinBar;
	incrementer incr;
	public int multiplier = 1;
	public Image img;

	public Text scoreText;

	void Start(){
		incr = GetComponent<incrementer>();
		StartCoroutine(updateScore());
	}

	public void incrementCoinText(){
		coinText.text = totalCoins.ToString();
	}

	public void incrementCoin(){
		totalCoins = totalCoins + (1 * multiplier);
	}

	IEnumerator updateScore(){
		score = score + (3 * (int)incr.speed);
		scoreText.text = score.ToString();
		yield return new WaitForSeconds(0.3f);
		StartCoroutine(updateScore());
	}

	public IEnumerator multiplierDuration(){
		yield return new WaitForSeconds(5);
		img.rectTransform.DOAnchorPosX(-88, 0.5f).SetEase(Ease.InCubic);
		multiplier = 1;
	}

	public void multiplierMoveIn(){
		img.rectTransform.DOAnchorPosX(25, 0.5f).SetEase(Ease.OutCubic);
	}

	public void AddCoinPoints(){
		score = score + 100;
	}





}
