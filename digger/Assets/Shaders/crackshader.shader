// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:1,cusa:True,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:True,tesm:0,olmd:1,culm:2,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:True,atwp:True,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:1873,x:33229,y:32719,varname:node_1873,prsc:2|emission-6472-RGB,clip-1547-OUT;n:type:ShaderForge.SFN_Tex2d,id:4918,x:32429,y:33056,ptovrint:False,ptlb:node_4918,ptin:_node_4918,varname:node_4918,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:a2eb9daeb78ff664abc42c7029acf9bb,ntxv:0,isnm:False;n:type:ShaderForge.SFN_TexCoord,id:1738,x:31634,y:33345,varname:node_1738,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_RemapRange,id:8478,x:31914,y:33341,varname:node_8478,prsc:2,frmn:0,frmx:1,tomn:-1,tomx:1|IN-1738-UVOUT;n:type:ShaderForge.SFN_Length,id:4276,x:32181,y:33330,varname:node_4276,prsc:2|IN-5986-UVOUT;n:type:ShaderForge.SFN_Floor,id:8554,x:32671,y:33326,varname:node_8554,prsc:2|IN-9775-OUT;n:type:ShaderForge.SFN_Slider,id:6835,x:32079,y:33633,ptovrint:False,ptlb:crackSize,ptin:_crackSize,varname:node_6835,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.9780049,max:1;n:type:ShaderForge.SFN_Add,id:9775,x:32420,y:33357,varname:node_9775,prsc:2|A-4276-OUT,B-6835-OUT;n:type:ShaderForge.SFN_OneMinus,id:6633,x:32890,y:33361,varname:node_6633,prsc:2|IN-8554-OUT;n:type:ShaderForge.SFN_Multiply,id:3283,x:32790,y:33116,varname:node_3283,prsc:2|A-4918-A,B-6633-OUT;n:type:ShaderForge.SFN_UVTile,id:5986,x:32148,y:33175,varname:node_5986,prsc:2|UVIN-8478-OUT,WDT-4047-OUT,HGT-4407-OUT,TILE-6093-OUT;n:type:ShaderForge.SFN_Vector1,id:4407,x:31737,y:33156,varname:node_4407,prsc:2,v1:1.4;n:type:ShaderForge.SFN_Vector1,id:4047,x:31833,y:33077,varname:node_4047,prsc:2,v1:1.4;n:type:ShaderForge.SFN_Vector1,id:6093,x:31820,y:33238,varname:node_6093,prsc:2,v1:0;n:type:ShaderForge.SFN_Subtract,id:1547,x:32873,y:32903,varname:node_1547,prsc:2|A-6472-A,B-3283-OUT;n:type:ShaderForge.SFN_Tex2d,id:6472,x:32815,y:32592,ptovrint:False,ptlb:MainTexture,ptin:_MainTexture,varname:node_6472,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:44a054df1702e39458b7072b08d0f212,ntxv:0,isnm:False;proporder:4918-6835-6472;pass:END;sub:END;*/

Shader "Shader Forge/crackshader" {
    Properties {
        _node_4918 ("node_4918", 2D) = "white" {}
        _crackSize ("crackSize", Range(0, 1)) = 0.9780049
        _MainTexture ("MainTexture", 2D) = "white" {}
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
        _Stencil ("Stencil ID", Float) = 0
        _StencilReadMask ("Stencil Read Mask", Float) = 255
        _StencilWriteMask ("Stencil Write Mask", Float) = 255
        _StencilComp ("Stencil Comparison", Float) = 8
        _StencilOp ("Stencil Operation", Float) = 0
        _StencilOpFail ("Stencil Fail Operation", Float) = 0
        _StencilOpZFail ("Stencil Z-Fail Operation", Float) = 0
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
            "CanUseSpriteAtlas"="True"
            "PreviewType"="Plane"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Cull Off
            
            
            Stencil {
                Ref [_Stencil]
                ReadMask [_StencilReadMask]
                WriteMask [_StencilWriteMask]
                Comp [_StencilComp]
                Pass [_StencilOp]
                Fail [_StencilOpFail]
                ZFail [_StencilOpZFail]
            }
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #pragma multi_compile _ PIXELSNAP_ON
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _node_4918; uniform float4 _node_4918_ST;
            uniform float _crackSize;
            uniform sampler2D _MainTexture; uniform float4 _MainTexture_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos( v.vertex );
                #ifdef PIXELSNAP_ON
                    o.pos = UnityPixelSnap(o.pos);
                #endif
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                float4 _MainTexture_var = tex2D(_MainTexture,TRANSFORM_TEX(i.uv0, _MainTexture));
                float4 _node_4918_var = tex2D(_node_4918,TRANSFORM_TEX(i.uv0, _node_4918));
                float node_4047 = 1.4;
                float node_6093 = 0.0;
                float2 node_5986_tc_rcp = float2(1.0,1.0)/float2( node_4047, 1.4 );
                float node_5986_ty = floor(node_6093 * node_5986_tc_rcp.x);
                float node_5986_tx = node_6093 - node_4047 * node_5986_ty;
                float2 node_8478 = (i.uv0*2.0+-1.0);
                float2 node_5986 = (node_8478 + float2(node_5986_tx, node_5986_ty)) * node_5986_tc_rcp;
                float node_4276 = length(node_5986);
                float node_9775 = (node_4276+_crackSize);
                float node_6633 = (1.0 - floor(node_9775));
                float node_3283 = (_node_4918_var.a*node_6633);
                clip((_MainTexture_var.a-node_3283) - 0.5);
////// Lighting:
////// Emissive:
                float3 emissive = _MainTexture_var.rgb;
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #pragma multi_compile _ PIXELSNAP_ON
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _node_4918; uniform float4 _node_4918_ST;
            uniform float _crackSize;
            uniform sampler2D _MainTexture; uniform float4 _MainTexture_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos( v.vertex );
                #ifdef PIXELSNAP_ON
                    o.pos = UnityPixelSnap(o.pos);
                #endif
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                float4 _MainTexture_var = tex2D(_MainTexture,TRANSFORM_TEX(i.uv0, _MainTexture));
                float4 _node_4918_var = tex2D(_node_4918,TRANSFORM_TEX(i.uv0, _node_4918));
                float node_4047 = 1.4;
                float node_6093 = 0.0;
                float2 node_5986_tc_rcp = float2(1.0,1.0)/float2( node_4047, 1.4 );
                float node_5986_ty = floor(node_6093 * node_5986_tc_rcp.x);
                float node_5986_tx = node_6093 - node_4047 * node_5986_ty;
                float2 node_8478 = (i.uv0*2.0+-1.0);
                float2 node_5986 = (node_8478 + float2(node_5986_tx, node_5986_ty)) * node_5986_tc_rcp;
                float node_4276 = length(node_5986);
                float node_9775 = (node_4276+_crackSize);
                float node_6633 = (1.0 - floor(node_9775));
                float node_3283 = (_node_4918_var.a*node_6633);
                clip((_MainTexture_var.a-node_3283) - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
